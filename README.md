# sbt-ci

sbt-ci is a docker image for sbt tasks (we use it for our Gitlab runner).
The image is based on Ubuntu:20.04 and contains
- AdoptOpenJDK 11
- sbt
- docker.io

If you have a use for docker (e.g. via sbt-native-packager) then pass environment variables
as described here
[docker-cli#environment-variables](https://docs.docker.com/engine/reference/commandline/cli/#environment-variables)

default workspace is `/opt/workspace`
default user is `gitlab-runner` with uid `1001`

