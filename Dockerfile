FROM ubuntu:20.04

RUN apt-get update && apt-get install wget curl apt-transport-https gnupg -y && \
	wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - && \
	echo "deb https://adoptopenjdk.jfrog.io/adoptopenjdk/deb focal main" | tee /etc/apt/sources.list.d/adoptopenjdk.list && \
	apt update && \
	apt install adoptopenjdk-11-hotspot -y

RUN echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list && \
	apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823 && \
	apt update -y && \
	apt install sbt -y

RUN apt install docker.io -y

RUN curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose

RUN rm -rf /var/lib/apt/lists/*

# Add a new user "gitlab-runner" with user id 1001
RUN useradd -m -u 1001 gitlab-runner
# Change to non-root privilege
USER gitlab-runner

WORKDIR /opt/workspace

